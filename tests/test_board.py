import sys
sys.path
sys.path.append('src/')

import pytest
from board import Board
from remi import Remi
from orientation import Orientation

# --------------------------------
#
#           Testing Rémi
#
# --------------------------------

def test_simple_move_remi():

    board = Board()
    remi = Remi()

    remi.update_pos_remi([0, 0])
    remi.update_remi(Orientation.SOUTH)

    board.move_remi(remi, 3)

    assert remi.get_pos() == [3, 0]


def test_remi_top_left():

    board = Board()
    remi = Remi()

    remi.update_pos_remi([0, 0])
    remi.update_remi(Orientation.NORTH)

    board.move_remi(remi, 1)

    assert remi.get_pos() == [0, 1]



# --------------------------------
#
#       Testing connex part
#
# --------------------------------


def test_connex_simple_square_several_positions():

    index_connex = [[0, 0], [0, 1], [1, 0], [1, 1]]
    offsets = [0, 1, 2]

    for offset in offsets:
    
        board = Board()
        for pos in index_connex:
            board.tiles[offset + pos[0], offset + pos[1]] = 1

        #tests
        for pos in index_connex:
            pos_offset = [offset + pos[0], offset + pos[1]]
            size = board.get_connex_paper(pos_offset, 1)
            assert size == 4


def test_connex_ell1_donnut():

    # positions donuts
    index_connex = [[0, 0], [0, 1], [0, 2], \
        [1, 0], [1, 2], \
        [2, 0], [2, 1], [2, 2]]
   
    board = Board()
    for pos in index_connex:
        board.tiles[pos[0], pos[1]] = 1

    #tests
    for pos in index_connex:
        size = board.get_connex_paper(pos, 1)
        assert size == 8


def test_connex_dna():

    # positions dna
    index_connex = [[0, 0], [0, 2], \
        [1, 0], [1, 1], [1, 2], \
        [2, 1], \
        [3, 0], [3, 1], [3, 2],
        [4, 0], [4, 2]]
   
    board = Board()
    for pos in index_connex:
        board.tiles[pos[0], pos[1]] = 1

    #tests
    for pos in index_connex:
        size = board.get_connex_paper(pos, 1)
        assert size == 11


