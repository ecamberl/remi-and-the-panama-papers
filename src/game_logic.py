from board import Board
from remi import Remi
from game_exceptions import *

import random


def assert_move_remi_is_legit(remi, new_orientation):
    diff = abs(remi.orient - new_orientation)
    if diff == 2:
        raise OrientationException


def roll_dice():
    dice = random.randint(1,6)
    if dice == 1:
        return 1
    elif dice <= 3:
        return 2
    elif dice <= 5:
        return 3
    else:
        return 4


def assert_paper_position_is_legit(board, remi, new_paper):
    #1. Check if both position are on the board
    if not board.check_tile_in_board(new_paper[0]):
	    raise PaperPositionException

    if not board.check_tile_in_board(new_paper[1]):
        raise PaperPositionException

    #2. Assert that position are continuous
    if not board.check_tiles_are_contiguous(new_paper[0], new_paper[1]):
        raise PaperPositionException

    #3. Assess that paper is next to remi
    remi_pos = remi.get_pos()
    out_bool = False
    if board.check_tiles_are_contiguous(new_paper[0], remi_pos):
        return

    if board.check_tiles_are_contiguous(new_paper[1], remi_pos):
        return

    raise PaperPositionException

    # and... it's done
