"""
Remi class, gives position and orientation of Remi on the board
"""

from orientation import Orientation

class Remi():
    """
    attributes:
        pos: a 1x2 list that places Remi on the board
    """
    def __init__(self, pos=[3,3], orient=Orientation.SOUTH):
        # Initial position is the middle of the board
        self.pos = pos
        self.orient = orient

    def copy(self):
        return Remi(pos=self.pos, orient=self.orient)

    def get_pos(self):
        # Since it is a 2 elements list, I prefer to copy it
        # and thus avoid bad surprises...
        # BUGGED
        return self.pos.copy()

    def get_orient(self):
        return self.orient

    def update_remi(self, orient):
       self.orient = orient
       
    def update_pos_remi(self,pos):
       self.pos = pos
