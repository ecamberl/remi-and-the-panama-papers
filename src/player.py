from orientation import Orientation
import random
from remi import Remi

import utils_ia

class Player(object):
    def __init__(self, name='dummy', idx=0, cits=0, shut_up=False):
        super(Player, self).__init__()
        self.name = name
        self.idx = idx
        self.citations = cits
        self.shut_up = shut_up

        self.speak("Here I am!! I am the Player " + str(self.idx))

    def speak(self, s):
        if self.shut_up:
            print(self.name + ': \"' + s + "\"")
        #print("I (proudly) have " + str(self.citations) + " citations on my google sholar profile :)")

    def speak_num(self,num):
        print(self.name + ": ", num)

    def get_direction(self, remi, board, wallets):
        """
        Return the new direction of Remi.
        This move is the first step of a plater turn

        Parameters
        ----------
        remi: Remi
            Remi
        board: Board
            an instance of the Board class
        wallets: list
            a list of size nbPlayer containing
            the number of citations of each player
        Returns
        -------
        orientation: Orientation
            the new orientation which should be legit
        """
        raise NotImplementedError


    def get_random_direction(self, remi):
        """
        Random version of get_direction.
        Will be called if something wrong happens:
            - an error occurs in your code :(
            - if we caught you cheating!

        Parameters
        ----------
        remi: Remi
            an instance of the Remi class
        Returns
        -------
        orientation: Orientation
            the new orientation which should be legit
        """
        while True:
            dice = random.randint(0,3)
            if abs(dice - remi.orient) != 2:
                return Orientation(dice)


    def place_paper(self, remi, board, wallets):
        """
        Place a paper. Second move of each turn.

        Parameters
        ----------
        remi: Remi
            an instance of the Remi class
        board: Board
            an instance of the Board class
        wallets: list
            a list of size nbPlayer containing
            the number of citations of each player
        Returns
        -------
        paper_position: list
            a list of two position.
            ex: [[0,0], [0,1]]
        """
        raise NotImplementedError


    def place_paper_random(self, remi, board):
        """
        Random version of place a paper.
        Will be called if something wrong happens:
            - an error occurs in your code :(
            - if we caught you cheating!

        Parameters
        ----------
        remi: Remi
            an instance of the Remi class
        board: Board
            an instance of the Board class
        Returns
        -------
        paper_position: list
            a list of two position.
            ex: [[0,0], [0,1]]
        """
        # 1: get all possible paper position
        possib = utils_ia.possible_place(remi.get_pos(), board.size)

        # 2: chose a random row in this matrix
        paper_idx = random.choice(possib)

        # 3: return the chosen position
        return paper_idx
