from src import player
from utils_ia import *
from remi import *
from board import *

def directionAdmissibles(positionActuelle):
    admissible1 = (positionActuelle +3) %4
    admissible2 = (positionActuelle +1) %4
    admissible3 = positionActuelle
    return [admissible1, admissible2, admissible3]

def Otherplayerid(idx):
        return (idx +1) %2

# evalutation de l'esperance de cout pour chaque orientation admissible
def AdmissibleOrientationsCost(OrientationsAdmissibles, playeridx, remi,board):
    DiceProb=(2,1,1,2)

    Cost=[0,0,0,0]

    for orientation in OrientationsAdmissibles:
        for tirageDé in range (0,4):
            #position pour un tirage et une orientation
            Position,newOrientation =predict_move(remi.get_pos(),orientation,tirageDé)
            tilePlayer=board.get_tile_player(Position)
            #print('tilePlayer = ',tilePlayer, 'Otherplayerid =',Otherplayerid(playeridx),'playeridx =',playeridx)


            if (tilePlayer == Otherplayerid(playeridx)) :
                #print('get_connex_paper = ',board.get_connex_paper(Position,Otherplayerid(playeridx)))
                Cost[orientation] += DiceProb[tirageDé]*board.get_connex_paper(Position,Otherplayerid(playeridx))
    return Cost

def BestAdmissibleCost(playeridx, remi,board):
    #definit les orientations possibles
    OrientationsAdmissibles = directionAdmissibles(remi.get_orient())
	
    # evalutation de l'esperance de cout pour chaque orientation admissible
    Cost=AdmissibleOrientationsCost(OrientationsAdmissibles,playeridx,remi,board)
    print("Cost")
    print(Cost)
    BestCost=max(Cost)
    BestOrientation= OrientationsAdmissibles[0]

    for orientation in OrientationsAdmissibles:
        if (Cost[orientation] < BestCost) :
            BestCost=Cost[orientation]
            BestOrientation= orientation
    print('BestCost = ',BestCost)
    print('BestOrientation = ',BestOrientation)
    return BestCost,BestOrientation

class Player(player.Player):
    def __init__(self, name='my team', idx=0, cits=0, shut_up=False):
        super(Player, self).__init__(name=name, idx=idx, cits=cits, shut_up=shut_up)

 
    def get_direction(self, remi, board, wallets):
	
        BestCost,BestOrientation = BestAdmissibleCost(self.idx, remi, board)
      
        print("REV TEAM PLAYING Orientation: ")
        print(BestOrientation)
        return BestOrientation
	
        #raise NotImplemented
        

    def place_paper(self, remi, board, wallets):
        # Get positions possibles de tapis
        places = possible_place(remi.get_pos())

        # pour chaque position de tapis, calcul du nouveau board + calculer le meilleur cout possible pour l'adversaire
        otherPlayerCost=0
        bestPlace=places[0]
        for place in places:
            #calcul du board en ajoutant un tapis
            #newBoard = ajoutTapis(board, place)
            newBoard = board
            newBoard.add_paper(place,self.idx)

            BestCost,BestOrientation = BestAdmissibleCost(Otherplayerid(self.idx), remi, newBoard)
            if (BestCost > otherPlayerCost) :
                bestPlace=place

        return bestPlace





