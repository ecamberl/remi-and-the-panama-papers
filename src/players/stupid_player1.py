from src import player


class Player(player.Player):
    def __init__(self, name='dummy', idx=0, cits=0, shut_up=False):
        super(Player, self).__init__(name=name, idx=idx, cits=cits, shut_up=shut_up)
