# define Python user-defined exceptions
class GameException(Exception):
   """Base class for other exceptions"""
   pass

class OrientationException(GameException):
   """Raised remi's move is not legit"""
   pass

class PaperPositionException(GameException):
   """Raised remi's move is not legit"""
   pass
