As a quick remainder, you need to implement:

1/ player.get_direction(self,remi,board,wallets)

This is the function that provides the orientation of Remi you want to play

2/ player.place_paper(self,remi,board,wallets)

This is the function that gives a list of 2 lists with 2 entries each,
containing the positions of the paper you want to place on the board.

Two example (random) such function are already implemented as
place_paper_random and get_random_direction.

--------------
Some Game info
--------------

Direction: North-East-South-West are coded as 0-1-2-3


-----------
API Summary
-----------

Some helpful functions:

Player API:
-----------

1/ speak([string] s): use this to write some message (taunt your ennemies!)

2/ get_next_tiles([list of list] indices, [Board] board): use this function to get all the
tiles next (no diagonal) to the two tiles in indices, as well as their current labels (color of the tiles). 

3/ possible_place([list] indexes): Returns all the possible list of list of
indexes where papers can be put, when playing from position 'indexes'. THIS IS
VERY USEFUL

4/ predict_moves([list] pos, [Orient] orient, [int] dice): given a position
pos, an orientation orient (in [0,3]) and a movement quantity dice, this
function predicts where you land on the board. THIS IS VERY USEFUL.

Remi API
--------

1/ get_pos(): return the position of remi as a list of two integers.

2/ get_orient(): return the orientation of remi

Board API
---------

1/ get_tile_player(pos): return the color (player index) on tile 'pos'

2/ get_connex_paper(pos, player_idx): computes the number of tiles next to each
other starting from pos, ONLY if the tile in pos is player_idx (otherwise it is
bugged and returns 1). THIS IS VERY, VERY USEFUL

3/ check_tile_in_board(pos): checks if a tile position is in the board or not.

4/  check_tiles_are_contiguous(til1,til2): checks if two tiles are contiguous

